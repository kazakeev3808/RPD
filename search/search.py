import tkinter
from tkinter import Canvas, Frame, Scrollbar, StringVar, ttk, messagebox
#Работа с файлами библеотека
from docx import Document
from docxtpl import DocxTemplate
from tkinter.constants import BOTH, END, LEFT, NW, RIGHT, SINGLE, Y, YES
#База Данных бибелотека
from pymongo import MongoClient
from config import token

doc = DocxTemplate("шаблон.docx")
document = Document('шаблон.docx')

cluster = MongoClient(token)
db = cluster["RPD"]
collection = db["ADMIN"]
#Возвращение в главный экран
def destroy():
    root.destroy()
def func():
    context = {
        'year': year.get(),
        'protokol': protokol.get(),
        'name_disciplins': name_disciplins.get(),
    }
    id = collection.find_one(context)
    if id  is None:
        messagebox.showerror("Ошибка", "Нету данного документа в базе")
    else:
        #Скрыть год, протокол и десциплтну
        search_year_in_base.place_forget()
        search_protokol_in_base.place_forget()
        search_name_disciplins_in_base.place_forget()
        year.place_forget()
        protokol.place_forget()
        name_disciplins.place_forget()
        search_in_base.place_forget()
        #Расположение работы с базой 
        day.place(x=0, y=40)
        month.place(x=0, y=80)
        focus.place(x=0, y=120)
        cells.place(x=0, y=160)
        tasks.place(x=0, y=200)
        academic.place(x=0, y=240)
        academic1.place(x=0, y=280)
        academic2.place(x=0, y=320)
        academic3.place(x=0, y=360)
        academic4.place(x=0, y=400)
        time_discipns_edinic.place(x=0, y=440)
        hour.place(x=0, y=480)
        hour_exam.place(x=0, y=520)
        osnov_literature.place(x=0, y=560)
        dop_litetature.place(x=0, y=600)
        period_literature.place(x=0, y=640)
        test.place(x=0, y=680)
        question.place(x=0, y=720)
        nomer_zd.place(x=0, y=760)
        #день
        day_in_db = collection.find_one(context)['day']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=40)
        for i in day_in_db:
            lis.insert(END, i)
            v1 = StringVar()
            lis_day = ttk.Entry(frame, textvariable=v1)
            lis_day.place(x=240, y=40)
            v1.set('123')
        #Месяц
        month_in_db = collection.find_one(context)['month']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=80)
        for i in month_in_db:
            lis.insert(END, i)
        lis_month = ttk.Entry(frame)
        lis_month.place(x=240, y=80)
        #год
        year_in_db = collection.find_one(context)['year']
        lis = ttk.Combobox(frame, state="readonly")
        for i in year_in_db:
            lis.insert(END, i)
        lis_year = ttk.Entry(frame)
        #protokol
        protokol_in_db = collection.find_one(context)['protokol']
        lis = ttk.Combobox(frame, state="readonly")
        for i in protokol_in_db:
            lis.insert(END, i)
        lis_protokol = ttk.Entry(frame)
        #name_disciplins
        name_disciplins_in_db = collection.find_one(context)['name_disciplins']
        lis = ttk.Combobox(frame, state="readonly")
        for i in name_disciplins_in_db:
            lis.insert(END, i)
        lis_name_disciplins = ttk.Entry(frame)
        #focus_of_training
        focus_of_training = collection.find_one(context)['focus_of_training']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=120)
        for i in focus_of_training:
            lis.insert(END, i)
        lis_focus_of_training = ttk.Entry(frame)
        lis_focus_of_training.place(x=240, y=120)
        #cells
        cells_in_db = collection.find_one(context)['cells']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=160)
        for i in cells_in_db:
            lis.insert(END, i)
        lis_cells = ttk.Entry(frame)
        lis_cells.place(x=240, y=160)
        #tasks
        tasks_in_db  = collection.find_one(context)['tasks']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=200)
        for i in tasks_in_db:
            lis.insert(END, i)
        lis_tasks = ttk.Entry(frame)
        lis_tasks.place(x=240, y=200)
        #academic
        academic_in_db  = collection.find_one(context)['academic']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=240)
        for i in academic_in_db:
            lis.insert(END, i)
        lis_academic = ttk.Entry(frame)
        lis_academic.place(x=240, y=240)
        #academic1
        academic1_in_db  = collection.find_one(context)['academic1']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=280)
        for i in academic1_in_db:
            lis.insert(END, i)
        lis_academic1 = ttk.Entry(frame)
        lis_academic1.place(x=240, y=280)
        #academic2
        academic2_in_db  = collection.find_one(context)['academic2']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=320)
        for i in academic2_in_db:
            lis.insert(END, i)
        lis_academic2 = ttk.Entry(frame)
        lis_academic2.place(x=240, y=320)
        #academic3
        academic3_in_db  = collection.find_one(context)['academic3']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=360)
        for i in academic3_in_db:
            lis.insert(END, i)
        lis_academic3 = ttk.Entry(frame)
        lis_academic3.place(x=240, y=360)
        #academic4
        academic4_in_db  = collection.find_one(context)['academic4']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=400)
        for i in academic4_in_db:
            lis.insert(END, i)
        lis_academic4 = ttk.Entry(frame)
        lis_academic4.place(x=240, y=400)
        #time_discipns_edinic
        time_discipns_edinic_in_db  = collection.find_one(context)['time_discipns_edinic']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=440)
        for i in time_discipns_edinic_in_db:
            lis.insert(END, i)
        lis_time_discipns_edinic = ttk.Entry(frame)
        lis_time_discipns_edinic.place(x=240, y=440)
        #hour
        hour_in_db  = collection.find_one(context)['hour']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=480)
        for i in hour_in_db:
            lis.insert(END, i)
        lis_hour = ttk.Entry(frame)
        lis_hour.place(x=240, y=480)
        #hour_exam
        hour_exam_in_db  = collection.find_one(context)['hour_exam']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=520)
        for i in hour_exam_in_db:
            lis.insert(END, i)
        lis_hour_exam = ttk.Entry(frame)
        lis_hour_exam.place(x=240, y=520)
        #osnov_literature
        osnov_literature_in_db  = collection.find_one(context)['osnov_literature']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=560)
        for i in osnov_literature_in_db:
            lis.insert(END, i)
        lis_osnov_literature = ttk.Entry(frame)
        lis_osnov_literature.place(x=240, y=560)
        #dop_litetature
        dop_litetature_in_db  = collection.find_one(context)['dop_litetature']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=600)
        for i in dop_litetature_in_db:
            lis.insert(END, i)
        lis_dop_litetature = ttk.Entry(frame)
        lis_dop_litetature.place(x=240, y=600)
        #period_literature
        period_literature_in_db  = collection.find_one(context)['period_literature']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=640)
        for i in period_literature_in_db:
            lis.insert(END, i)
        lis_period_literature = ttk.Entry(frame)
        lis_period_literature.place(x=240, y=640)
        #test
        test_in_db  = collection.find_one(context)['test']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=680)
        for i in test_in_db:
            lis.insert(END, i)
        lis_test = ttk.Entry(frame)
        lis_test.place(x=240, y=680)
        #question
        question_in_db  = collection.find_one(context)['question']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=720)
        for i in question_in_db:
            lis.insert(END, i)
        lis_question = ttk.Entry(frame)
        lis_question.place(x=240, y=720)
        #nomer_zd
        nomer_zd_in_db  = collection.find_one(context)['nomer_zd']
        lis = ttk.Combobox(frame)
        lis.place(x=240, y=760)
        for i in nomer_zd_in_db:
            lis.insert(END, i)
        lis_nomer_zd = ttk.Entry(frame)
        lis_nomer_zd.place(x=240, y=760)
        #Обработа введенных новых данных
        def processing():
            year_get = id['year']
            protokol_get = id['protokol']
            name_disciplins_get = id['name_disciplins']
            context = {
                'day': lis_day.get(),
                'month': lis_month.get(),
                'year': year_get,
                'protokol': protokol_get,
                'name_disciplins': name_disciplins_get,
                'focus_of_training': lis_focus_of_training.get(),
                'cells': lis_cells.get(),
                'tasks': lis_tasks.get(),
                'academic':lis_academic.get(),
                'academic1':lis_academic1.get(),
                'academic2':lis_academic2.get(),
                'academic3':lis_academic3.get(),
                'academic4':lis_academic4.get(),
                'time_discipns_edinic':lis_time_discipns_edinic.get(),
                'hour':lis_hour.get(),
                'hour_exam':lis_hour_exam.get(),
                'osnov_literature':lis_osnov_literature.get(),
                'dop_litetature':lis_dop_litetature.get(),
                'period_literature':lis_period_literature.get(),
                'test':lis_test.get(),
                'question':lis_question.get(),
                'nomer_zd':lis_nomer_zd.get(),
            }
            #Загрузка в баззу
            collection.insert(context)
            #Изменяем документ
            doc.render(context)
            doc.save("Готовый документ.docx")
            root.destroy()
        #Кнопка с изменением 
        edit = tkinter.Button(root, text="Изменить", width=20, height=2, command=processing)
        edit.place(x=482, y=450) 
def visible_true():
    #Скрытие работ с базой
    day.place_forget()
    month.place_forget()
    focus.place_forget()
    cells.place_forget()
    tasks.place_forget()
    academic.place_forget()
    academic1.place_forget()
    academic2.place_forget()
    academic3.place_forget()
    academic4.place_forget()
    time_discipns_edinic.place_forget()
    hour.place_forget()
    hour_exam.place_forget()
    osnov_literature.place_forget()
    dop_litetature.place_forget()
    period_literature.place_forget()
    test.place_forget()
    question.place_forget()
    nomer_zd.place_forget()
    #Расположение года, протокола и десциплтны
    search_year_in_base.place(x=0, y=40)
    search_protokol_in_base.place(x=0, y=60)
    search_name_disciplins_in_base.place(x=0, y=80)    
    search_in_base.place(x=0, y=100)

root = tkinter.Tk()

root.title("Редактирование шаблона")
#Размеры
width = 640
height = 500
x = 100
y = 0
root.geometry(f"{width}x{height}+{x}+{y}")
#Цвет
root["bg"] = "gray22"
frame1 = Frame(root, width=250, height=100)
frame1.place(x=10, y=10)
#Скроллбар
canvas = Canvas(frame1)
canvas.config(width=600, height=430)
canvas.config(scrollregion=(0, 0, 300, 3660))
sbar = Scrollbar(frame1)
sbar.config(command=canvas.yview)
sbar.pack(side=RIGHT, fill=Y)
canvas.config(yscrollcommand=sbar.set)
canvas.pack(side=LEFT, expand=YES, fill=BOTH)

frame = Frame(canvas, width=3000, height=2920)
canvas.create_window((0, 0), window=frame, anchor=NW)
#Проверка по базе
year = tkinter.Entry(frame, width=5)
year.place(x=250, y=40)
protokol = tkinter.Entry(frame, width=5)
protokol.place(x=250, y=60)
name_disciplins = tkinter.Entry(frame, width=20)
name_disciplins.place(x=250, y=80)
#Кнопка по проверке базу данных
search_in_base = tkinter.Button(root, text='Найти в базе', width=20, height=2, command=func)
search_in_base.place(x=482, y=450)
#Кнопка с возврашение в главный экран
back = tkinter.Button(root, text="Вернуться назад", width=20, height=2, command=destroy)   
back.place(x=12,y=450)
#Проверка по базе данных
search_year_in_base = tkinter.Label(frame, text="Введите год:")
search_year_in_base.place(x=0, y=40)
search_protokol_in_base =tkinter.Label(frame, text="номер протокола:")
search_protokol_in_base.place(x=0, y=60)
search_name_disciplins_in_base =tkinter.Label(frame, text="Наименование дисциплины (модуля):")
search_name_disciplins_in_base.place(x=0, y=80)  
#После проверки
day =tkinter.Label(frame, text="Введите день:")
month = tkinter.Label(frame, text="Введите месяц:")
focus = tkinter.Label(frame, text="Введите focus_of_training:")
cells = tkinter.Label(frame, text="Введите cells:")
tasks = tkinter.Label(frame, text="Введите tasks:")
academic = tkinter.Label(frame, text="Введите academic:")
academic1 = tkinter.Label(frame, text="Введите academic1:")
academic2 = tkinter.Label(frame, text="Введите academic2:")
academic3 = tkinter.Label(frame, text="Введите academic3:")
academic4 = tkinter.Label(frame, text="Введите academic4:")
time_discipns_edinic = tkinter.Label(frame, text="Введите time_discipns_edinic:")
hour = tkinter.Label(frame, text="Введите hour:")
hour_exam = tkinter.Label(frame, text="Введите hour_exam:")
osnov_literature = tkinter.Label(frame, text="Введите osnov_literature:")
dop_litetature = tkinter.Label(frame, text="Введите dop_litetature:")
period_literature = tkinter.Label(frame, text="Введите period_literature:")
test = tkinter.Label(frame, text="Введите test:")
question = tkinter.Label(frame, text="Введите question:")
nomer_zd = tkinter.Label(frame, text="Введите nomer_zd:")

root.mainloop()