from docx import Document
from docxtpl import DocxTemplate
from tkinter import *
from tkinter.scrolledtext import ScrolledText
import openpyxl
from openpyxl import load_workbook
from subprocess import call
import subprocess
from pymongo import MongoClient
from search.config import token

doc = DocxTemplate("шаблон.docx")
document = Document('шаблон.docx')


# book = load_workbook('book.xlsm')


cluster = MongoClient(token)
db = cluster["RPD"]
collection = db["ADMIN"]

def Open():
    call(["python", ".\search\search.py"])


def clicked():
    context = {'day': day.get(), 'month': month.get(), 'year': year.get(), 'protokol': protokol.get(),
               'name_disciplins': name_disciplins.get(), 'focus_of_training': focus_of_training.get(),
               'cells': cells.get(1.0, END), 'tasks': tasks.get(1.0, END), 'academic': academic.get(1.0, END),
               'academic1': a_academic.get(1.0, END), 'academic2': academic2.get(1.0, END),
               'academic3': academic3.get(1.0, END),
               'academic4': academic4.get(1.0, END), 'time_discipns_edinic': time_discipns_edinic.get(),
               'hour': hour.get(), 'hour_exam': hour_exam.get(),
               'osnov_literature': osnov_litetature.get(1.0, END),
               'dop_litetature': dop_litetature.get(1.0, END), 'period_literature': period_litetature.get(1.0, END),
               'test': test.get(1.0, END), 'question': question.get(1.0, END), 'nomer_zd': nomer_zd.get(),
               'zd': zd.get(1.0, END)}
    doc.render(context)
    doc.save(q)
    collection.insert(context)
    window.destroy()


def clear():
    day.delete(0, END)
    month.delete(0, END)
    year.delete(0, END)
    protokol.delete(0, END)
    name_disciplins.delete(0, END)
    focus_of_training.delete(0, END)
    cells.delete(1.0, END)
    tasks.delete(1.0, END)
    academic.delete(1.0, END)
    a_academic.delete(1.0, END)
    academic2.delete(1.0, END)
    academic3.delete(1.0, END)
    academic4.delete(1.0, END)
    time_discipns_edinic.delete(0, END)
    hour.delete(0, END)
    hour_exam.delete(0, END)
    osnov_litetature.delete(1.0, END)
    dop_litetature.delete(1.0, END)
    period_litetature.delete(1.0, END)
    test.delete(1.0, END)
    question.delete(1.0, END)
    nomer_zd.delete(0, END)
    zd.delete(1.0, END)
    global s, s1, s2, s, s3, s4, s5, s6
    s, s1, s2, s3, s4, s5, s6 = 1, 1, 1, 1, 1, 1, 1


def new_doc():
    def clear_doc():
        docx.delete(0, END)

    def s_doc():
        global q
        q = docx.get() + '.docx'
        qq = open(docx.get() + '.docx', 'w')
        qq.close()

        a.destroy()

    a = Toplevel()
    a.geometry('300x140+270+180')
    a.resizable(width=False, height=False)
    Label(a, text="Укажите имя новому документу,\n"
                  "или укажите имя уже имеющегося документа").place(x=20, y=0)
    Label(a, text="Имя:").place(x=0, y=50)

    docx = Entry(a, width=25)
    docx.place(x=40, y=50)
    Label(a, text=".docx").place(x=185, y=50)
    Button(a, text="Ввести", width=19, height=2, command=s_doc).place(x=5, y=85)
    Button(a, text="очистить", width=19, height=2, command=clear_doc).place(x=153, y=85)


# функция table1 непригодна к работе, оставить с заделом на будущее
def table1():
    filename = "book.xlsm"
    wsToOpen = "table1"
    xlsx_file = openpyxl.load_workbook('book.xlsm')
    lst_worksheets = xlsx_file.get_sheet_names()
    xlsx_file._active_sheet_index = lst_worksheets.index(wsToOpen)
    xlsx_file.save(filename)
    subprocess.Popen([filename], shell=True)


s = 1


def num(event):
    global s
    tasks.insert(END, "{}.".format(s))
    s += 1


s1 = 1


def num1(event):
    global s1
    osnov_litetature.insert(END, "{}.".format(s1))
    s1 += 1


s2 = 1


def num2(event):
    global s2
    dop_litetature.insert(END, "{}.".format(s2))
    s2 += 1


s3 = 1


def num3(event):
    global s3
    period_litetature.insert(END, "{}.".format(s3))
    s3 += 1


s4 = 1


def num4(event):
    global s4
    test.insert(END, "{}.".format(s4))
    s4 += 1


s5 = 1


def num5(event):
    global s5
    question.insert(END, "{}.".format(s5))
    s5 += 1


s6 = 1


def num6(event):
    global s6
    question.insert(END, "{}.".format(s6))
    s6 += 1


window = Tk()

window.title("Заполнение шаблона")
width = 640
height = 500
x = 100
y = 0
window.geometry(f"{width}x{height}+{x}+{y}")
window.resizable(width=False, height=False)
window["bg"] = "gray22"
window.geometry(f"{width}x{height}+{x}+{y}")
frame1 = Frame(window, width=250, height=100)
frame1.place(x=10, y=10)

canvas = Canvas(frame1)
canvas.config(width=600, height=430)
canvas.config(scrollregion=(0, 0, 300, 2960))
sbar = Scrollbar(frame1)
sbar.config(command=canvas.yview)
sbar.pack(side=RIGHT, fill=Y)
canvas.config(yscrollcommand=sbar.set)
canvas.pack(side=LEFT, expand=YES, fill=BOTH)

frame = Frame(canvas, width=3000, height=2920)
canvas.create_window((0, 0), window=frame, anchor=NW)

# Оглавление
Label(frame, text="Заполнение шаблона документа").place(x=150, y=0)
# Заполняемый лист
Label(frame, text="Титульный лист").place(x=200, y=20)

# Дата(число, месяц, год)
Label(frame, text="Введите дату:").place(x=0, y=40)
Label(frame, text="<<").place(x=228, y=40)
Label(frame, text=">>").place(x=288, y=40)
day = Entry(frame, width=5)
day.place(x=250, y=40)

month = Entry(frame, width=10)
month.place(x=310, y=40)

year = Entry(frame, width=5)
year.place(x=395, y=40)
Label(frame, text="20").place(x=375, y=40)

# Протокол документа, наименование и направленность дисциплины
Label(frame, text="номер протокола:").place(x=0, y=60)
protokol = Entry(frame, width=5)
protokol.place(x=250, y=60)

Label(frame, text="Наименование дисциплины (модуля):").place(x=0, y=80)
name_disciplins = Entry(frame, width=47)
name_disciplins.place(x=250, y=80)

Label(frame, text="Направленность (профиль) подготовки:").place(x=0, y=100)
focus_of_training = Entry(frame, width=47)
focus_of_training.place(x=250, y=100)

# Глава I.ЦЕЛИ И ЗАДАЧИ ДИСЦИПЛИНЫ (МОДУЛЯ)
Label(frame, text="I. ЦЕЛИ И ЗАДАЧИ ДИСЦИПЛИНЫ (МОДУЛЯ)").place(x=150, y=120)

Label(frame, text="Цели:").place(x=0, y=140)
cells = ScrolledText(frame, wrap=WORD)
cells.place(x=250, y=140, width=300, height=100)

Label(frame, text="Задачи:").place(x=0, y=240)
tasks = ScrolledText(frame, wrap=WORD)
tasks.place(x=250, y=250, width=300, height=250)
tasks.bind("<Control_L>", num)

Label(frame, text="II. МЕСТО ДИСЦИПЛИНЫ В СТРУКТУРЕ ОПОП ВО").place(x=150, y=510)

Label(frame, text="Учебная дисциплина относится к части:").place(x=0, y=530)
academic = ScrolledText(frame, wrap=WORD)
academic.place(x=250, y=530, width=300, height=180)

Label(frame, text="Необходимые дисциплины,\n для освоения учебной дисциплиной:", justify=LEFT).place(x=0, y=730)
a_academic = ScrolledText(frame, wrap=WORD)
a_academic.place(x=250, y=730, width=300, height=180)

Label(frame, text="Знания и навыки,\n"
                  "полученные обучающимися\n"
                  "в курсе дисциплины,\n"
                  "могут оказаться полезными\n"
                  "при изучении следующих предметов", justify=LEFT).place(x=0, y=920)
academic2 = ScrolledText(frame, wrap=WORD)
academic2.place(x=250, y=920, width=300, height=90)
Label(frame, text="Для изучения данной\n"
                  "учебной дисциплины (модуля)\n"
                  "необходимы знания, умения и навыки:", justify=LEFT).place(x=0, y=1030)
academic3 = ScrolledText(frame, wrap=WORD)
academic3.place(x=250, y=1030, width=300, height=90)
Label(frame, text="Перечень последующих учебных\n"
                  "дисциплин, для которых необходимы\n"
                  "знания, умения и навыки:", justify=LEFT).place(x=0, y=1140)
academic4 = ScrolledText(frame, wrap=WORD)
academic4.place(x=250, y=1140, width=300, height=90)
Label(frame, text="III. ТРЕБОВАНИЯ К РЕЗУЛЬТАТАМ ОСВОЕНИЯ ДИСЦИПЛИНЫ").place(x=150, y=1250)

Label(frame, text="Перечень планируемых результатов\n"
                  "обучения по дисциплине (модулю),\n"
                  "соотнесенных с индикаторами\n"
                  "достижения компетенций:", justify=LEFT).place(x=0, y=1270)
Button(frame, text="Заполнить", width=30, height=3, command=table1).place(x=260, y=1280)

Label(frame, text="IV. СОДЕРЖАНИЕ И СТРУКТУРА ДИСЦИПЛИНЫ").place(x=150, y=1360)

Label(frame, text="Трудоемкость дисциплины составляет:").place(x=0, y=1380)
Label(frame, text="Зачётных единиц:").place(x=250, y=1380)
time_discipns_edinic = Entry(frame, width=4)
time_discipns_edinic.place(x=355, y=1380)
Label(frame, text="Часов:").place(x=250, y=1400)
hour = Entry(frame, width=4)
hour.place(x=355, y=1400)
Label(frame, text="Часов на экзамен:").place(x=250, y=1420)
hour_exam = Entry(frame, width=4)
hour_exam.place(x=355, y=1420)
Label(frame, text="Форма экзамена:").place(x=0, y=1440)
form_exam = Entry(frame, width=21)
form_exam.place(x=250, y=1440)

Label(frame, text="4.1 Содержание дисциплины,\n"
                  "структурированное по темам,\n"
                  "c указанием видов учебных занятий\n"
                  "и отведенного на них количества\n"
                  "академических часов", justify=LEFT).place(x=0, y=1470)
Button(frame, text="Заполнить", width=30, height=4, command=table1).place(x=260, y=1470)

Label(frame, text="4.2 План внеаудиторной\n"
                  "самостоятельной работы\n"
                  "обучающихся по дисциплине", justify=LEFT).place(x=0, y=1570)
Button(frame, text="Заполнить", width=30, height=3, command=table1).place(x=260, y=1570)

Label(frame, text="4.3 Содержание\n"
                  "учебного материала", justify=LEFT).place(x=0, y=1650)
Button(frame, text="Заполнить", width=30, height=2, command=table1).place(x=260, y=1650)

Label(frame, text="4.3.1. Перечень семинарских,\n"
                  "практических занятий и\n"
                  "лабораторных работ", justify=LEFT).place(x=0, y=1700)
Button(frame, text="Заполнить", width=30, height=2, command=table1).place(x=260, y=1705)

Label(frame, text="4.3.2. Перечень тем (вопросов),\n"
                  "выносимых на самостоятельное изучение\n"
                  "самостоятельной работы студентов", justify=LEFT).place(x=0, y=1770)
Button(frame, text="Заполнить", width=30, height=3, command=table1).place(x=260, y=1770)

Label(frame, text="4.5. Примерная тематика\n"
                  "курсовых работ (проектов)").place(x=0, y=1830)

topic_course = ScrolledText(frame, wrap=WORD)
topic_course.place(x=250, y=1830, width=300, height=100)

Label(frame, text="V. УЧЕБНО-МЕТОДИЧЕСКОЕ И ИНФОРМАЦИОННОЕ\n"
                  "ОБЕСПЕЧЕНИЕ ДИСЦИПЛИНЫ (МОДУЛЯ)").place(x=150, y=1960)

Label(frame, text="основная литература").place(x=0, y=2000)
osnov_litetature = ScrolledText(frame, wrap=WORD)
osnov_litetature.place(x=250, y=2000, width=300, height=100)
osnov_litetature.bind("<Control_L>", num1)

Label(frame, text="дополнительная литература").place(x=0, y=2120)
dop_litetature = ScrolledText(frame, wrap=WORD)
dop_litetature.place(x=250, y=2120, width=300, height=100)
dop_litetature.bind("<Control_L>", num2)

Label(frame, text="периодическая литература").place(x=0, y=2240)
period_litetature = ScrolledText(frame, wrap=WORD)
period_litetature.place(x=250, y=2240, width=300, height=100)
period_litetature.bind("<Control_L>", num3)

Label(frame, text="VII. ОБРАЗОВАТЕЛЬНЫЕ ТЕХНОЛОГИИ").place(x=150, y=2360)
Label(frame, text="Наименование тем занятий\n"
                  "с использованием активных\n"
                  "форм обучения:", justify=LEFT).place(x=0, y=2390)

Button(frame, text="Заполнить", width=30, height=2, command=table1).place(x=260, y=2390)

Label(frame, text="VIII. ОЦЕНОЧНЫЕ МАТЕРИАЛЫ ДЛЯ\n"
                  "ТЕКУЩЕГО КОНТРОЛЯ И\n"
                  "ПРОМЕЖУТОЧНОЙ АТТЕСТАЦИИ").place(x=150, y=2460)
Label(frame, text="8.1. Оценочные средства\n"
                  "текущего контроля", justify=LEFT).place(x=0, y=2530)

Button(frame, text="Заполнить", width=30, height=2, command=table1).place(x=260, y=2530)

Label(frame, text="Демонстрационный вариант теста").place(x=0, y=2590)
test = ScrolledText(frame, wrap=WORD)
test.place(x=250, y=2590, width=300, height=100)
test.bind("<Control_L>", num4)

Label(frame, text="Примерный перечень вопросов\n"
                  "и заданий к экзамену (зачету)\n"
                  "Вопросы к экзамену.", justify=LEFT).place(x=0, y=2710)
question = ScrolledText(frame, wrap=WORD)
question.place(x=250, y=2710, width=300, height=100)
question.bind("<Control_L>", num5)

Label(frame, text="Номер примера задания:").place(x=0, y=2820)
nomer_zd = Entry(frame, width=3)
nomer_zd.place(x=250, y=2820)

Label(frame, text="Пример задания").place(x=0, y=2840)
zd = ScrolledText(frame, wrap=WORD)
zd.place(x=250, y=2840, width=300, height=100)
zd.bind("<Control_L>", num6)

Button(window, text="Создать новый документ", width=20, height=2, command=new_doc).place(x=10, y=450)
Button(window, text="Заполнить", width=18, height=2, command=clicked).place(x=180, y=450)
Button(window, text="Очистить", width=18, height=2, command=clear).place(x=320, y=450)
Button(window, text="Редактировать документ", width=20, height=2, command=Open).place(x=482, y=450)

window.mainloop()
